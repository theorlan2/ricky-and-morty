import React, { FunctionComponent, useEffect, useState } from 'react';
import { Character } from '../../infrastructure/models/character/Character';
import Card from '../Card/Card';

type ResultCharactersProps = {
 results: Character[];
 page: string;
}

const ResultCharacters: FunctionComponent<ResultCharactersProps> = ({ results, page }) => {

    const [showResults, setShowResults] = useState(false);
    const [characters, setCharacters] = useState([] as Character[]);

    useEffect(() => {
        let haveResults =  results && results.length > 0;      
        setShowResults(haveResults);
        if(haveResults){
          setCharacters(results);
        }
    }, [results]);
    

return <div className='row' >
   {!showResults && <p>No Characters Found :/</p>}
{showResults && characters.map((item: Character) => <Card id={item.id} page={page} key={item.id+'-character'} result={item} />)}
  </div>;
}

export default ResultCharacters;