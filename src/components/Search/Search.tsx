import React, { EventHandler, FunctionComponent } from 'react';
import styles from './Search.module.scss';

type PropsSearch = {
    setSearch: (text: string) => void
    updatePageNumber: (pageNumber: number) => void
} 

const Search: FunctionComponent<PropsSearch> = (props) => {

function searchBtn(e: any) {
    e.preventDefault(); 
    
}
    
    return <form className={`${styles.search} d-flex flex-sm-row flex-column align-items-center justify-content-center gap-4 mb-5 `} >
        <input onChange={(e) =>{
            props.updatePageNumber(1);
            props.setSearch(e.target.value);
        }}
        placeholder='Search for characters'
        className={styles.input}
        type="text"
        />
        <button onClick={searchBtn} className={`${styles.btn} btn btn-primary fs-5`} >
        Search
        </button>
    </form>;
}

export default Search;
