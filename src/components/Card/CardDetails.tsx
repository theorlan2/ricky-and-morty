import React, { FunctionComponent, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'
import { Character } from '../../infrastructure/models/character/Character'
import DefaultLayout from '../Layout/Default';

const CardDetails: FunctionComponent = (props) => {
    const { id } = useParams();
    let [fetchedData, updateFetchedData] = useState({} as Character);
    let { name, location, origin, gender, image, status, species } = fetchedData;
    let api = `https://rickandmortyapi.com/api/character/${id}`;

    useEffect(() => {
        (async function () {
            let data = await fetch(api).then((res) => res.json());
            updateFetchedData(data);
        })();
    }, [api]);

    return <div>
        <h1 className='text-center mt-4' >Character Details</h1>
        <DefaultLayout
            filterSearch={<div className='col-md-3'></div>}>
            <div className="container d-flex justify-content-center mb-5">
                <div className="d-flex flex-column gap-3">
                    <h1 className="text-center">{name}</h1>
                    <img className="img-fluid" src={image} alt="" />
                    {(() => {
                        if (status === "Dead") {
                            return <div className="badge bg-danger fs-5">{status}</div>;
                        } else if (status === "Alive") {
                            return <div className=" badge bg-success fs-5">{status}</div>;
                        } else {
                            return <div className="badge bg-secondary fs-5">{status}</div>;
                        }
                    })()}

                    <div className="content">
                        <div className="">
                            <span className="fw-bold">Gender : </span>
                            {gender}
                        </div>
                        <div className="">
                            <span className="fw-bold">Location: </span>
                            {location?.name}
                        </div>
                        <div className="">
                            <span className="fw-bold">Origin: </span>
                            {origin?.name}
                        </div>
                        <div className="">
                            <span className="fw-bold">Species: </span>
                            {species}
                        </div>
                    </div>
                </div>

            </div>
        </DefaultLayout>


    </div>;

}

export default CardDetails;
