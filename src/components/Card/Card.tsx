import React, { FunctionComponent, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Character } from '../../infrastructure/models/character/Character';
import styles from './Card.module.scss';

type PropsCard = {
    result: Character;
    page: string;
    id: number;
    
}

const Card: FunctionComponent<PropsCard> = (props) => {
    const [statusClass, setStatusClass] = useState('');

    useEffect(() => {
        determinateStatus(props.result.status);
    },[])

    function determinateStatus(status: string) {
        if (status === "Dead") {
            setStatusClass('bg-danger')
        } else if (status === "Alive") {            
            setStatusClass('bg-success')
        } else {
            setStatusClass('bg-secondary')
        }
    }
        return <Link
        style={{ textDecoration: "none" }}
        to={`${props.page}${props.id}`}
        key={props.id}
        className="col-lg-4 col-md-6 col-sm-6 col-12 mb-4 position-relative text-dark"
      >
            <div className={`${styles.card} d-flex flex-column justify-content-center`}>
                <div className={`${styles.badge} position-absolute badge ${statusClass}`}>
                    {props.result.status}
                </div>
                <img className={`${styles.img} img-fluid`} src={props.result.image} alt="" />
                <div className={`${styles.content}`}>
                    <div className="fs-5 fw-bold mb-4">
                        {props.result.name}
                    </div>
                    <div className="">
                        <div className="fs-6 fw-normal">
                            Last Location
                        </div>
                        <div className="fs-5">
                            {props.result.location.name}
                        </div>
                    </div>
                </div>
            </div>
        </Link>;
    }

    export default Card;
