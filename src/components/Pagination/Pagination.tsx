import React, { FunctionComponent, useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';

type PropsPagination = {
    info: any,
    pageNumber: number;
    updatePageNumber: (pNumber: number) => void;
}

const PaginationComponent: FunctionComponent<PropsPagination> = (props) => {

    const [width, setWidth] = useState(window.innerWidth);
    const updateDimensions = () => {
        setWidth(window.innerWidth);
    };
    useEffect(() => {
        window.addEventListener("resize", updateDimensions);
        return () => window.removeEventListener("resize", updateDimensions);
    }, []);

    function pageChange(data: { selected: number }) {
        props.updatePageNumber(data.selected + 1);
    }



    return (
        <>
            <style >
                {`
          a {
            color: white; text-decoration: none;
          }
          @media (max-width: 768px) {
            .pagination {font-size: 12px}
        
            .next,
            .prev {display: none}
          }
          @media (max-width: 768px) {
            .pagination {font-size: 14px}
          }
        `}
            </style>
            <ReactPaginate
                forcePage={props.pageNumber === 1 ? 0 : props.pageNumber - 1}
                marginPagesDisplayed={width < 576 ? 1 : 2}
                pageRangeDisplayed={width < 576 ? 1 : 2}
                pageCount={props.info?.pages}
                onPageChange={pageChange}
                className="pagination justify-content-center my-4 gap-4"
                nextLabel="Next"
                previousLabel="Prev"
                previousClassName="btn btn-primary fs-5 prev"
                nextClassName="btn btn-primary fs-5 next"
                activeClassName="active"
                pageClassName="page-item"
                pageLinkClassName="page-link"
            />
        </>
    );
}

export default PaginationComponent;
