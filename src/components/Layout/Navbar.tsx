import { NavLink, Link } from "react-router-dom";

import React, { FunctionComponent } from 'react';

const Navbar: FunctionComponent = (props) => {
  return <nav className="navbar navbar-expend-lg navbar-light bg-light mb-4" >
    <div className="container">
      <div className="row col-12">
        <div className="col-md-6">
          <Link to='/' className='navbar-brand fs-3 ubuntu'  >
            Rick & Morty <span className="text-primary" >Wiki</span>
          </Link>
        </div>
        <div className="navbar-nav fs-5 col-md-6  justify-content-end">
          <div className="row">

            <div className="col">
              <NavLink to="/" className="nav-link">
                Characters
              </NavLink>
            </div>
            <div className="col">
              <NavLink to="/episodes" className="nav-link">
                Episode
              </NavLink>
            </div>
            <div className="col">
              <NavLink
                className="nav-link"
                to="/location" >Location</NavLink>
            </div>
          </div>
        </div>
      </div>
      <div
        className="collapse navbar-collapse"
        id="navbarNavAltMarkup"
      > <div className="navbar-nav fs-5">
          <NavLink to="/" className="nav-link">
            Characters
          </NavLink>
          <NavLink to="/episodes" className="nav-link">
            Episode
          </NavLink>
          <NavLink
            className="nav-link"
            to="/location" >Location</NavLink>
        </div>
      </div>
      <style >
        {`
                button[aria-expanded="false"] > .close {
display:none;
                }
                button[aria-expanded="true"] > .open {
                    display:none;
                }
            `}
      </style>
    </div>
  </nav>;
}


export default Navbar