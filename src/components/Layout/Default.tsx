import React, { FunctionComponent, ReactElement } from 'react';
import Navbar from './Navbar';

type PropsDefaultLayout ={ 
    filterSearch: ReactElement;
}

const DefaultLayout:FunctionComponent<PropsDefaultLayout> = (props) => {

  return  <div className="container" > 
    <button
  className="navbar-toggler border-0"
  type="button"
  data-bs-toggle="collapse"
  data-bs-target="#navbarNavAltMarkup"
  aria-controls="navbarNavAltMarkup"
  aria-expanded="false"
  aria-label="Toggle navigation"
>
  <span className='fas fa-bars open text-dark'></span>
  <span className='fas fa-times close text-dark'></span>
</button>
<Navbar /> 
    <div className="row">
      {props.filterSearch}
      <div className="col-lg-8 col-12 ">
        {props.children}
      </div>
    </div>
  </div>;
}

export default DefaultLayout;