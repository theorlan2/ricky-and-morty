import React, { FunctionComponent } from 'react';
import { CategoryEnum } from '../../infrastructure/enums/CategoryEnum';

type PropsFilter = {
    input: string;
    categoryId: number;
    index: number;
    name: string;
    task:(input: string,id:CategoryEnum) => void;
    updatePageNumber: (page: number) => void;
}

const FilterBtn: FunctionComponent<PropsFilter> = (props) => {

    return <div>
        <style >
            {`
        .x:checked + label {
            background-color: #0b5ed7;
            color: white
            }
            input[type="radio"] { display:none; }
        }
    `}
        </style>

        <div className="form-check">
            <input type="radio" className="form-check-input x" name={props.name} id={`${props.name}-${props.index}`} />
            <label htmlFor="" onClick={(x) => {
                props.task(props.input,props.categoryId); props.updatePageNumber(1);
            }} className='btn btn-outline-primary' >{props.input}</label>
        </div>
    </div> 
}

export default FilterBtn;
