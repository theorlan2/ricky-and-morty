import React, { FunctionComponent } from 'react';
import AccordionItem from './AccordionItem';
import { CategoryEnum } from '../../../../infrastructure/enums/CategoryEnum';


type PropsFilterCategories = {
    update: (categoryName: string, id: CategoryEnum) => void;
    updatePageNumber: (page: number) => void;
}

const AccordionCategories: FunctionComponent<PropsFilterCategories> = (props) => {
    const categories = [
        {
            id: CategoryEnum.status,
            title: 'Status',
            name: 'status',
            items: ["Alive", "Dead", "Unknown"],
        },
        {
            id: CategoryEnum.species,
            title: 'Species',
            name: 'species',
            items: [
                "Human", "Alien", "Humanoid",
                "Poopybutthole", "Mythological", "Unknown",
                "Animal", "Disease", "Robot", "Cronenberg", "Planet",],
        },
        {
            id: CategoryEnum.gender,
            title: 'Genders',
            name: 'gender',
            items: ["female", "male", "genderless", "unknown"]
        },


    ]

    return <div className="accodion" id='accordionExample'>
        {categories.map((item, indexCategory) =>
            <AccordionItem
                key={`${indexCategory}-filter-category`}
                name={item.name}
                title={item.title}
                categoryId={item.id}
                index={indexCategory}
                items={item.items}
                update={props.update} updatePageNumber={props.updatePageNumber}
            />)}
    </div>;
}

export default AccordionCategories;
