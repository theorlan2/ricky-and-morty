import React, { FunctionComponent } from 'react';
import { CategoryEnum } from '../../../../infrastructure/enums/CategoryEnum';
import FilterBtn from '../../FilterBtn';


type PropsFilterCategory = {
    name: string;
    title: string;
    categoryId: CategoryEnum;
    items: string[];
    index: number;
    update: (categoryName: string,id:CategoryEnum) => void;
    updatePageNumber: (page: number) => void;
}

const AccordionItem: FunctionComponent<PropsFilterCategory> = (props) => {
 

    return <div className='acordion-item' >
        <h2 className='accordion-header' id='headingOne' >
            <button className='accordion-button' type='button' data-bs-toggle='collapse' data-bs-target={`#collapse-${props.index}`}
                aria-expanded='true' aria-controls={`collapse-${props.index}`} >{props.title}</button>
        </h2>
        <div id={`collapse-${props.index}`} className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">

            <div className="accordion-body d-flex flex-wrap gap-3">
                {props.items.map((item, index: number) =>
                    (<FilterBtn key={index + '-filter'}  categoryId={props.categoryId} index={index} name={props.name} task={props.update} updatePageNumber={props.updatePageNumber} input={item} />))}
            </div>

        </div>
    </div>;
}

export default AccordionItem;
