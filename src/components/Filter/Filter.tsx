import React, { FunctionComponent } from 'react';
import { CategoryEnum } from '../../infrastructure/enums/CategoryEnum';
import AccordionCategories from './category/accordion/AccordionCategories';

type PropsFilter = {
    pageNumber: number
    status: string
    updateStatus: (status: string) => void;
    updateGender: (status: string) => void;
    updateSpecies: (status: string) => void;
    updatePageNumber: (page: number) => void;
}

const Filter: FunctionComponent<PropsFilter> = (props) => {

    function clear() {
        props.updateStatus('');
        props.updateGender('');
        props.updateSpecies('');
        props.updatePageNumber(1);
        // @ts-ignore
        window.location.reload(false);
    }

    function update(categoryName: string, categoryId: CategoryEnum) {
        switch (categoryId) {
            case CategoryEnum.status:
                props.updateStatus(categoryName);
                break;
            case CategoryEnum.gender:
                props.updateGender(categoryName);
                break;
            case CategoryEnum.species:
                props.updateSpecies(categoryName);
                break;
            default:
                break;
        }
    }

    return <div className='col-lg-3 col-12 mb-5' >
        <div className="text-center fw-bold fs-4 mb-2">Filters</div>
        <div
        onClick={clear}
            style={{ cursor: "pointer" }}
            className="text-primary text-decoration-underline text-center mb-3">
            Clear Filter
        </div>

        <AccordionCategories
            updatePageNumber={props.updatePageNumber}
            update={update}
        />
    </div>;
}

export default Filter;
