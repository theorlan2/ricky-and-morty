import React, { FunctionComponent } from 'react';

type InputGroupProps = {
    changeID: (value: number) => void
    name: string;
    total: number;
}

const InputGroup: FunctionComponent<InputGroupProps> = (props) => {

    return <div className="input-group mb-3">
        <select
            onChange={(e) => props.changeID(+e.target.value)}
            className="form-select"
            id={props.name}
        >
            <option value="1">Choose...</option>
            {[...Array(props.total).keys() as any].map((x, index) => {
                return (
                    <option value={x + 1}>
                        {props.name} - {x + 1}
                    </option>
                );
            })}
        </select>
    </div>

}

export default InputGroup;
