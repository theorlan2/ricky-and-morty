import React, { useEffect, useState } from 'react';
import { Route, Routes } from 'react-router-dom';
import Home from '../pages/Home';
import Episodes from "../pages/Episodes";
import Location from "../pages/Location";
import CardDetails from '../components/Card/CardDetails';

function App() {

  return (
    <div className="App">
      <Routes  >
        <Route path="/" element={<Home />} />
        <Route path="/episodes" element={<Episodes />} />
        <Route path="/location" element={<Location />} />
        <Route path="/:id" element={<CardDetails />} />
        <Route path="/episodes/:id" element={<CardDetails />} />
        <Route path="/location/:id" element={<CardDetails />} />
      </Routes>
    </div>
  );
}

export default App;
