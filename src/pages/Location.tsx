import React, { FunctionComponent, useEffect, useState } from 'react';
import { Character } from '../infrastructure/models/character/Character'; 
import ResultCharacters from '../components/resultCharacters/ResultCharacters'; 
import DefaultLayout from '../components/Layout/Default';
import InputGroup from '../components/Filter/InputGroup';

type PropsEpisodes = {

}

const Episodes: FunctionComponent<PropsEpisodes> = (props) => {

    const [results, setResults] = React.useState([] as Character[]);
    const [info, setInfo] = useState({} as any);
    const { dimension, type, name } = info;
    const [id, setId] = useState(1);

    let api = `https://rickandmortyapi.com/api/location/${id}`;

    useEffect(() => {
        (async function () {
            let data = await fetch(api).then((res) => res.json());
            setInfo(data);

            let characters = await Promise.all(
                data.residents.map((x: string) => {
                    return fetch(x).then((res) => res.json());
                })
            );
            setResults(characters);
        })();
    }, [api]);

    return <div>
        <h1 className="text-center mb-3" >Location</h1> 
        <DefaultLayout
            filterSearch={<div className='col-lg-3 col-12 mb-5' >
                <h4 className="text-center mb-4">Pick Location:</h4>
                <InputGroup name="Location" changeID={setId} total={51} />
            </div>
            }>
            <h1 className="text-center mb-3">
                Location:{" "}
                <span className="text-primary">{name === "" ? "Unknown" : name}</span>
            </h1> 
            <h5 className="text-center">
                Dimension: {dimension === "" ? "Unknown" : dimension}
            </h5>
            <h6 className="text-center">Type: {type === "" ? "Unknown" : type}</h6>
            <ResultCharacters page="/location/"  results={results} />
        </DefaultLayout>
    </div>;

}

export default Episodes;
