import React, { FunctionComponent, useEffect, useState } from 'react';
import { Character } from '../infrastructure/models/character/Character';
import { Pagination } from '../infrastructure/types/Generics/Pagination';
import ResultCharacters from '../components/resultCharacters/ResultCharacters';
import Search from '../components/Search/Search';
import PaginationComponent from '../components/Pagination/Pagination';
import DefaultLayout from '../components/Layout/Default';
import Filter from '../components/Filter/Filter';

type PropsHome = {

}

const Home: FunctionComponent<PropsHome> = (props) => {
    let [pageNumber, updatePageNumber] = useState(1);
    let [search, setSearch] = useState('');
    const [status, setStatus] = useState('');
    const [gender, setGender] = useState('');
    const [species, setSpecies] = useState('');

    let api = `https://rickandmortyapi.com/api/character/?page=${pageNumber}&name=${search}&status=${status}&gender=${gender}&species=${species}`;

    const [fetchedData, setFetchedData] = useState({} as Pagination<Character>);
    const { info, results } = fetchedData;


    useEffect(() => {
        (async () => {
            const data = await fetch(api).then(res => res.json())
            setFetchedData(data);
        })();
        console.log('re-render-home');
    }, [api]);

    return <div>
                <h1 className="text-center mb-3" >Characters</h1>
        <Search setSearch={setSearch} updatePageNumber={updatePageNumber} /> 
        <DefaultLayout
            filterSearch={<Filter
                pageNumber={pageNumber}
                status={status}
                updateStatus={setStatus}
                updateGender={setGender}
                updateSpecies={setSpecies}
                updatePageNumber={updatePageNumber}
                />}
                > 
            <ResultCharacters page="/"  results={results} />
            <PaginationComponent info={info} pageNumber={pageNumber} updatePageNumber={updatePageNumber} />
        </DefaultLayout>
    </div>;

}

export default Home;
