export type Pagination<T> = { 
    info: {
    count: number,
    pages: number,
    next: string | undefined,
    prev: string | undefined
},
results:T[]
}