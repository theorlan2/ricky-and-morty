export type Character = {
  id: number,
  name: string,
  status: string,
  species: string,
  type: string,
  gender: string,
  origin: GenericEntityUrl,
  location: GenericEntityUrl,
  image: string,
  episode: string[],
  url: string,
  created: string
}


export type GenericEntityUrl = {
    name: string,
    url: string
}